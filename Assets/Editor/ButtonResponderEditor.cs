﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ButtonResponder))]
public class ButtonResponderEditor : Editor {

    ButtonResponder m_buttonResponder;

    private void OnEnable()
    {
        m_buttonResponder = (ButtonResponder)target;
    }

    public override void OnInspectorGUI()
    {
        m_buttonResponder.NeverDisables = EditorGUILayout.Toggle("Never Disables", m_buttonResponder.NeverDisables);
        m_buttonResponder.ButtonFunction = (ButtonResponder.functions)EditorGUILayout.EnumPopup("Responder Type", m_buttonResponder.ButtonFunction);
        
        //DrawDefaultInspector();
    }

}
