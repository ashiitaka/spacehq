﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonResponder : MonoBehaviour {

    public bool NeverDisables;
    bool IsSelectedObject = false;
    Animator anims;
    Button button;

    public enum functions
    {
        Social,
        Leaderboards,
        Account,
        OpenNews,
        DeleteGame,
        WatchFullScreenAdvert,
        OpenShop,
        CloseShop,
        LoadAdventureMode,
        LoadTravelMode,
        CloseNews,
        ToggleMusic,
        ToggleSFX,
        OpenSettings,
        CloseSettings,
        OpenAdventure,
        CloseAdventure,
        OpenShipZoom,
        CloseShipZoom,
        KeepLocalData,
        KeepRemoteData,
        Count,
    }

    public functions ButtonFunction;

    private void OnEnable()
    {
        if(this.gameObject.GetComponent<Animator>())
        {
            anims = this.gameObject.GetComponent<Animator>();
        }
        if(this.GetComponent<Button>())
        {
            button = this.GetComponent<Button>();
            button.enabled = true;
        }
        else
        {
            if(!this.GetComponent<Player>())
            {
                Debug.LogError("No Button on this Responder ", this.gameObject);
            }           
        }
    }

    public void OnPress(int _variable)
    {
        AudioManager.instance.PlaySFX(AudioManager.SFX.ButtonSuccess);

        if(!NeverDisables)
        {
            button.enabled = false;
        }

        if(Application.isEditor)
        {
            Debug.Log(ButtonFunction.ToString());
        }

        switch (ButtonFunction)
        {
            case functions.Social:
                {
                    UIManager.instance.HideUI(); //turn off the UI
                    SocialManager.instance.sTwitter.Initialize();
                    SocialManager.instance.sTwitter.StartLogin();
                    break;
                }
            case functions.Leaderboards:
                {
                    if(!Application.isEditor)
                    {
                        Social.ReportScore(Game.Inventory.GalaMiles, "CgkI6qSb6-wUEAIQAA", (bool success) => {
                            // handle success or failure
                        });
                    }
                    Social.ShowLeaderboardUI();
                    break;
                }
            case functions.Account:
                {
                    Social.ShowAchievementsUI();
                    break;
                }
            case functions.OpenNews:
                {
                    Game.SwitchCamera(Game.availableCameras.Shop);
                    UIManager.instance.NewsUI.IsVisible = true;
                    break;
                }
            case functions.CloseNews:
                {
                    Game.SwitchCamera(Game.availableCameras.Main);
                    UIManager.instance.NewsUI.IsVisible = false;
                    break;
                }
            case functions.DeleteGame:
                {
                    Game.Data.AWSData.Credentials.Clear();

                    Game.Data.AWSData.LocalDataSet = Game.Data.AWSData.syncManager.OpenOrCreateDataset("PlayerData");

                    Game.Data.AWSData.LocalDataSet.Put("User Name", Social.localUser.userName);
                    Game.Data.AWSData.LocalDataSet.Put("User ID", Social.localUser.id);
                    Game.Data.AWSData.LocalDataSet.Put("GalaMiles", "0");

                    Game.Data.AWSData.LocalDataSet.SynchronizeAsync();
                    Game.Data.InitialisePlayer();
                    UIManager.instance.DataIssue.IsVisible = false;
                    break;
                }
            case functions.WatchFullScreenAdvert:
                {
                    Game.AdHandlerUnity.ShowRewardedVideo();
                    break;
                }
            case functions.OpenShop:
                {
                    Game.SwitchCamera(Game.availableCameras.Shop);
                    UIManager.instance.ShopUI.IsVisible = true;
                    break;
                }
            case functions.CloseShop:
                {
                    Game.SwitchCamera(Game.availableCameras.Main);
                    UIManager.instance.ShopUI.IsVisible = false;
                    break;
                }
            case functions.LoadAdventureMode:
                {
                    if(Application.isEditor)
                    {
                        Game.instance.LoadScene("Adventure");
                    }
                    else
                    {
                        UIManager.instance.ComingSoon.IsVisible = true;
                    }
                    break;
                }
            case functions.LoadTravelMode:
                {
                    Game.instance.LoadScene("Main");
                    break;
                }
            case functions.ToggleMusic:
                {
                    AudioManager.instance.ToggleMusic();
                    break;
                }
            case functions.ToggleSFX:
                {
                    AudioManager.instance.ToggleSFX();
                    break;
                }
            case functions.OpenSettings:
                {
                    UIManager.instance.SettingsUI.IsVisible = true;
                    Game.SwitchCamera(Game.availableCameras.Shop);
                    break;
                }
            case functions.CloseSettings:
                {
                    UIManager.instance.SettingsUI.IsVisible = false;
                    Game.SwitchCamera(Game.availableCameras.Main);
                    break;
                }
            case functions.OpenAdventure:
                {
                    Game.SwitchCamera(Game.availableCameras.WorldMap);
                    UIManager.instance.MainHUD.IsVisible = false;
                    UIManager.instance.WorldMapHUD.IsVisible = true;
                    UIManager.instance.AdventureUI.IsVisible = true;
                    break;
                }
            case functions.CloseAdventure:
                {
                    if(Game.State == Game.states.WorldMap)
                    {
                        Game.SwitchCamera(Game.availableCameras.Main);
                        UIManager.instance.MainHUD.IsVisible = true;
                        UIManager.instance.WorldMapHUD.IsVisible = false;
                        UIManager.instance.AdventureUI.IsVisible = false;
                        break;
                    }
                    if(Game.State == Game.states.WorldMapZoom)
                    {
                        WorldMapParent.instance.SelectPlanet(null);
                        break;
                    }
                    break;
                }
            case functions.OpenShipZoom:
                {
                    Game.SwitchCamera(Game.availableCameras.ShipZoom);
                    UIManager.instance.ShipZoomUI.IsVisible = true;
                    UIManager.instance.MainHUDButtons.IsVisible = false;
                    break;
                }
            case functions.CloseShipZoom:
                {
                    Game.SwitchCamera(Game.availableCameras.Main);
                    UIManager.instance.ShipZoomUI.IsVisible = false;
                    UIManager.instance.MainHUDButtons.IsVisible = true;
                    break;
                }
            case functions.KeepLocalData:
                {
                    Game.Data.AWSData.KeepLocalData();
                    break;
                }
            case functions.KeepRemoteData:
                {

                    break;
                }
            default:
                break;
        }
    }
}
