﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldMapPlanetHUD : MonoBehaviour {

    public TMPro.TextMeshProUGUI tTitle;

    [SerializeField]
    bool bisVisible;
    public bool bIsVisible
    {
        get
        {
            return bisVisible;
        }
        set
        {
            bisVisible = value;
            if (value)
            {
                gameObject.SetActive(value);
            }

            if (anims != null && anims.isActiveAndEnabled)
            {
                if (value && EnableAnimation != "")
                {
                    anims.SetTrigger(EnableAnimation);
                }
                else if (!value && DisableAnimation != "")
                {
                    anims.SetTrigger(DisableAnimation);
                }
            }
            else if (!value)
            {
                gameObject.SetActive(value);
            }
        }
    }

    public string EnableAnimation;
    public string DisableAnimation;
    Animator anims;

    public void Populate(string _sNewTitle)
    {
        tTitle.text = _sNewTitle;
    }

}
