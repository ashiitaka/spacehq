﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DynamicGrid : MonoBehaviour  {

    public enum gridTypes
    {
        SpaceshipShop,
        Count,
    }

    public GameObject HeaderTemplate;
    public GameObject Template;
    public gridTypes GridType;
    public List<GameObject> IgnoredObjects = new List<GameObject>();

    bool _selectedFirstEntry;

    void OnEnable ()
    {
        _selectedFirstEntry = false;
        if(Template!=null)
        {
            Template.SetActive(false);
        }

        if(HeaderTemplate != null)
        {
            HeaderTemplate.SetActive(false);
        }

        foreach (Transform t in this.transform)
        {
            if(t.gameObject.activeInHierarchy)
            {
                if(!IgnoredObjects.Contains(t.gameObject))
                {
                    Destroy(t.gameObject);
                }                
            }
        }
        switch (GridType)
        {
            case gridTypes.SpaceshipShop:
                string activeEvent = RemoteSettings.GetString("ActiveEvent", "");
                Debug.Log(activeEvent);
                for (int i = 0; i < StoreManager.instance.ShopShips.Count; i++)
                {
                    if (StoreManager.instance.ShopShips[i].LinkedEvent.ToString() == activeEvent ||
                        StoreManager.instance.ShopShips[i].LinkedEvent == EventManager.Events.None ||
                        StoreManager.instance.ShopShips[i].IsOwned == true)
                    {
                        PlayerShipScriptable s = (PlayerShipScriptable)StoreManager.instance.ShopShips[i];
                        GameObject t = Instantiate(Template, transform);
                        VariableShopObject vo = t.GetComponent<VariableShopObject>();
                        vo.Index = i;
                        vo.ItemImage.sprite = s.Sprite;
                        vo.ItemName.SetText(s.Name);

                        t.SetActive(true);                    }

                }
                break;
            default:
                break;

        }
    }

    void SelectFirstEntry(GameObject selection)
    {
        selection.GetComponent<Selectable>().Select();
        _selectedFirstEntry = true;
    }

}
