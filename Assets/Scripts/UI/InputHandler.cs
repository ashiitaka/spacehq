﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {

    public enum inputtypes
    {
        ShipName,
    }

    public inputtypes InputType;

    TMPro.TMP_InputField inputField;

    private void Awake()
    {
        if(GetComponent<TMPro.TMP_InputField>())
        {
            inputField = this.GetComponent<TMPro.TMP_InputField>();
        }        
    }
    private void OnEnable()
    {
        switch (InputType)
        {
            case inputtypes.ShipName:
                if(PlayerPrefs.HasKey("ShipName"))
                {
                    inputField.text = PlayerPrefs.GetString("ShipName");
                }
                else
                {
                    inputField.text = "Space HQ Ship";
                }
                break;
            default:
                break;
        }
    }

    public void SetText(string newText)
    {
        switch (InputType)
        {
            case inputtypes.ShipName:
                PlayerPrefs.SetString("ShipName", newText);
                break;
            default:
                break;
        }
    }
}
