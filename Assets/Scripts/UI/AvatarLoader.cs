﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class AvatarLoader : MonoBehaviour {

    public GameObject LoadingState;
    public Image LoadedImage;

    private void OnEnable()
    {
        LoadingState.SetActive(true);
    }

    private void FixedUpdate()
    {
        if (Social.localUser != null) //TODO make this into its own thing
        {
            if(Social.localUser.image != null)
            {
                LoadedImage.sprite = Sprite.Create(Social.localUser.image, new Rect(0, 0, Social.localUser.image.width, Social.localUser.image.height), Vector2.one / 2);
                LoadingState.SetActive(false);
            }            
        }
    }
}
