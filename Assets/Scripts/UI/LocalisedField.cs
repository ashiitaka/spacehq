﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LocalisedField : MonoBehaviour {

    int _index;
    TextMeshProUGUI textComponent;

    public enum textEntry
    {
        LocalDataSet,
        RemoteDataSet,
        RemoteNewsText,
        VersionNumber,
        MusicState,
        SFXState,
    }

    public textEntry TextDisplay;

    private void Awake()
    {
        textComponent = GetComponent<TextMeshProUGUI>();
        textComponent.text = "";

        RemoteSettings.Updated += new RemoteSettings.UpdatedEventHandler(RemoteSettings_Updated);
    }

    private void RemoteSettings_Updated()
    {
        if(TextDisplay == textEntry.RemoteNewsText)
        {
            textComponent.text = RemoteSettings.GetString("News", "Unable to recieve News!");
        }
    }

    public void Refresh()
    {
        OnEnable();
    }

    void OnEnable()
    {
        switch (TextDisplay)
        {
            case textEntry.LocalDataSet:
                {
                    if(Game.Data.AWSData !=null)
                    {
                        if (Game.Data.AWSData.LocalDataSet != null)
                        {
                            textComponent.text = "Local Dataset";
                            for (int i = 0; i < Game.Data.AWSData.LocalDataSet.Records.Count; i++)
                            {
                                textComponent.text = string.Concat(textComponent.text,"\n", Game.Data.AWSData.LocalDataSet.Records[i].Key, " . ", Game.Data.AWSData.LocalDataSet.Records[i].Value);
                            }
                            textComponent.text = string.Concat(textComponent.text, "\n", "Creation Date: ", Game.Data.AWSData.LocalDataSet.Metadata.CreationDate);
                        }
                    }
                    break;
                }
            case textEntry.RemoteDataSet:
                {
                    if (Game.Data.AWSData != null)
                    {
                        if (Game.Data.AWSData.RemoteDataSet != null)
                        {
                            textComponent.text = "Remote Dataset";
                            for (int i = 0; i < Game.Data.AWSData.RemoteDataSet.Records.Count; i++)
                            {
                                textComponent.text = string.Concat(textComponent.text, "\n", Game.Data.AWSData.RemoteDataSet.Records[i].Key, " . ", Game.Data.AWSData.RemoteDataSet.Records[i].Value);
                            }
                            textComponent.text = string.Concat(textComponent.text, "\n", "Creation Date: ", Game.Data.AWSData.RemoteDataSet.Metadata.CreationDate);
                        }
                    }
                    break;
                }
            case textEntry.RemoteNewsText:
                {
                    RemoteSettings_Updated();
                    break;
                }
            case textEntry.VersionNumber:
                {
                    textComponent.text = Application.version;
                    break;
                }
            case textEntry.MusicState:
                {
                    if(AudioManager.instance.MusicPlayer.mute)
                    {
                        textComponent.text = string.Concat("Music - OFF");
                    }
                    else
                    {
                        textComponent.text = string.Concat("Music - ON");
                    }
                    break;
                }
            case textEntry.SFXState:
                {
                    if (AudioManager.instance.IsSFXMuted)
                    {
                        textComponent.text = string.Concat("SFX - OFF");
                    }
                    else
                    {
                        textComponent.text = string.Concat("SFX - ON");
                    }
                    break;
                }
            default:
                break;
        }
    }
}
