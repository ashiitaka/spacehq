﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class VariableShopObject : VariableObject {

    public Image ItemImage;
    public GameObject Banner;
    public TextMeshProUGUI ItemName;
    public TextMeshProUGUI ItemDescription;
    public TextMeshProUGUI ItemCost;

    public enum objectType
    {
        Ship,
    }

    public objectType ItemType;

    private void OnEnable()
    {
        switch (ItemType)
        {
            case objectType.Ship:
                if(StoreManager.instance.ShopShips[Index].LinkedEvent != EventManager.Events.None)
                {
                    Banner.SetActive(true);
                }
                else
                {
                    Banner.SetActive(false);
                }

                CheckIfOwned();
                break;
            default:
                break;
        }
    }

    public void SetPlayerShip()
    {
        if(!CheckIfOwned())
        {
            if (ShopUtilities.PurchaseShip(Index))
            {
                ItemCost.SetText("Owned");
            }
            else
            {
                UIManager.instance.PurchaseFunnel.IsVisible = true;
            }
        }
        else
        {
            Game.Inventory.ShipIndex = Index;
        }
    }

    bool CheckIfOwned()
    {
        if (!StoreManager.instance.ShopShips[Index].IsOwned)
        {
            ItemCost.SetText(StoreManager.instance.ShopShips[Index].Cost.ToString());
            return false;
        }
        else
        {
            ItemCost.SetText("Owned");
            return true;
        }
    }
}
