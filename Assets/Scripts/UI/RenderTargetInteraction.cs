﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RenderTargetInteraction : MonoBehaviour, IPointerDownHandler
{
    public Camera RenderCamera;

    public virtual void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("down");
        List<RaycastResult> results = new List<RaycastResult>();
        UIManager.instance.MainHUD.GetComponentInParent<GraphicRaycaster>().Raycast(eventData, results);

        for (int i = 0; i < results.Count; i++)
        {
            if(results[i].gameObject.CompareTag("RenderTarget"))
            {
                TestCollision(results[i]);
            }
        }
    }

    void TestCollision (RaycastResult hit)
    {
        Vector2 localPoint = (Vector2)hit.screenPosition - (Vector2)hit.gameObject.transform.position;
        Debug.Log(localPoint);
        Ray portalRay = RenderCamera.ScreenPointToRay(new Vector2(localPoint.x, localPoint.y));
        //portalRay.direction = new Vector3(portalRay.direction.x, portalRay.direction.y, 10);
        RaycastHit portalHit;
        // test these camera coordinates in another raycast test
        if (Physics.Raycast(portalRay, out portalHit))
        {
            Debug.Log(portalHit.collider.gameObject);
        }
        Debug.DrawRay(portalRay.origin, portalRay.direction * 1000, Color.red,5 );
    }
}
