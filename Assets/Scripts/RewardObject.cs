﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardObject : MonoBehaviour {

    public SpriteRenderer rewardIcon;
    public TMPro.TextMeshPro textDisplay;
    public AudioClip RewardSFX;

    public void Init(string textToDisplay)
    {
        textDisplay.text = textToDisplay;
        if(RewardSFX != null)
        {
            AudioManager.instance.PlaySFX(RewardSFX);
        }        
    }
}
