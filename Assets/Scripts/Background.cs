﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    public bool StretchToFill;

	// Use this for initialization
	void Start ()
    {
        if(StretchToFill)
        {
            transform.localScale = new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight);
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
