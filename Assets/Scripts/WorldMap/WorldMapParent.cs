﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldMapParent : MonoBehaviour {

    public static WorldMapParent instance;
    public WorldMapPlanetHUD PlanetHUD;
    public List<WorldMapObject> Planets = new List<WorldMapObject>();
    public static WorldMapObject SelectedPlanet;
    Camera mainCam;

    public bool Highlighted = false;

    private void Awake()
    {
        instance = this;
        OnBecameVisible();
    }

    private void OnBecameVisible()
    {
        resetView();
        mainCam = Camera.main;
    }

    public void SelectPlanet(WorldMapObject newSelection)
    {
        SelectedPlanet = newSelection;

        for (int i = 0; i < Planets.Count; i++)
        {
            if(Planets[i] == newSelection)
            {
                SelectedPlanet = newSelection;
            }            
            else
            {
            }
        }

        if(SelectedPlanet == null)
        {
            resetView();
        }
        else
        {
            zoomOnSelectedPlanet();
        }
    }

    void resetView()
    {
        PlanetHUD.bIsVisible = false;
        Game.SwitchCamera(Game.availableCameras.WorldMap);
    }

    void zoomOnSelectedPlanet()
    {
        PlanetHUD.bIsVisible = true;
        PlanetHUD.Populate(SelectedPlanet.sFriendlyName);
        Game.SwitchCamera(Game.availableCameras.WorldMapZoom);
    }

    private void FixedUpdate()
    {
        if(Game.State == Game.states.WorldMap)
        {
            if (Input.GetMouseButtonDown(0) && !Highlighted)
            {
                SelectPlanet(null);
            }
        }

        if (SelectedPlanet != null)
        {
            PlanetHUD.transform.position = (Vector2)mainCam.WorldToScreenPoint(SelectedPlanet.transform.position);
        }

    }
}
