﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WorldMapObject : MonoBehaviour{

    public string sFriendlyName;
    public bool bIsInteractive;
    public Vector3 v3rotation;
    Vector3 v3initialScale;

    private void Start()
    {
        v3initialScale = transform.localScale;
    }

    private void OnMouseOver()
    {
        if (Game.State == Game.states.WorldMap && bIsInteractive)
        {
            Highlight();
        }        
    }

    private void OnMouseExit()
    {
        if (bIsInteractive)
        {
            UnHighlight();
        }
    }

    private void OnMouseUp()
    {
        if(Game.State == Game.states.WorldMap && bIsInteractive)
        {
            WorldMapParent.instance.SelectPlanet(this);
            UnHighlight();
        }        
    }


    void Highlight()
    {
        WorldMapParent.instance.Highlighted = true;
        transform.localScale = new Vector3(v3initialScale.x + 0.01f, v3initialScale.y + 0.01f, v3initialScale.z + 0.01f);
    }

    void UnHighlight()
    {
        WorldMapParent.instance.Highlighted = false;
        transform.localScale = v3initialScale;
    }

	// Update is called once per frame
	void FixedUpdate ()
    {
        transform.Rotate(v3rotation);		
	}
}
