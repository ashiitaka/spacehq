﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldControl : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
	    if(Input.GetKey(KeyCode.W))
        {
            this.transform.RotateAround(this.transform.localPosition, Vector3.left, 15 * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.transform.RotateAround(this.transform.localPosition, Vector3.right, 15 * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            this.transform.RotateAround(this.transform.localPosition, Vector3.down, 15 * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.transform.RotateAround(this.transform.localPosition, Vector3.up, 15 * Time.deltaTime);
        }
    }
}
