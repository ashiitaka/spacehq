﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Player : MonoBehaviour {

    public SpriteRenderer ShipImage;
    public GameObject OverlayObjectParent;
    GameObject overlayObject;
    public GameObject ExhaustEffectParent;
    GameObject exhaustObject;
    Vector3 startScale;

    public void init()
    {
        ShipImage = gameObject.AddComponent<SpriteRenderer>();
        SetShip(Game.Inventory.ShipIndex);
        startScale = this.transform.localScale;
    }

    private void OnMouseDown()
    {
        if(Game.State == Game.states.Main)
        {
            if(GetComponent<ButtonResponder>())
            {
                GetComponent<ButtonResponder>().OnPress(0);
            }
            else
            {
                Debug.LogError("Missing Button Responder on Player Object");
            }
        }
        
    }

    public void SetShip(int shipIndex)
    {
        if (overlayObject != null)
        {
            Destroy(overlayObject);
        }

        if (exhaustObject != null)
        {
            Destroy(exhaustObject);
        }

        PlayerShipScriptable s = (PlayerShipScriptable)StoreManager.instance.ShopShips[shipIndex];
        ShipImage.sprite = s.Sprite;

        if(s.OverlayObject != null)
        {
            overlayObject = Instantiate(s.OverlayObject, OverlayObjectParent.transform);
        }
        if(s.ExhaustEffect != null)
        {
            exhaustObject = Instantiate(s.ExhaustEffect, ExhaustEffectParent.transform);
        }
    }

    bool isVisible = true;
    public void IsVisible(bool newState)
    {
        isVisible = newState;
        if(newState)
        {
            gameObject.transform.localScale = startScale;
        }
        else
        {
            gameObject.transform.localScale = Vector3.zero;
        }
        if (overlayObject != null)
        {
            overlayObject.SetActive(newState);
        }

        if (exhaustObject != null)
        {
            exhaustObject.SetActive(newState);
        }
    }
}
