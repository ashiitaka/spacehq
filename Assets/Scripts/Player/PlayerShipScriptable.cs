﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewShip", menuName = "Player Ships")]
public class PlayerShipScriptable : ScriptableObject {

    public Sprite Sprite;
    public GameObject OverlayObject;
    public GameObject ExhaustEffect;
    public string Name;
    public string Description;
    public int Cost;

    public EventManager.Events LinkedEvent;
    public bool IsOwned;
}
