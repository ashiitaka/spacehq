﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoSync.SyncManager;
using Amazon.CognitoSync;

using GooglePlayGames;
using System;

public class Data_AWS {

    public CognitoAWSCredentials Credentials;
    public CognitoSyncManager syncManager;
    public Dataset LocalDataSet;
    public Dataset RemoteDataSet;

    public string GoogleToken;

    public void InitializeAmazonWebServices()
    {
        UIManager.instance.LoadingText.text = "Checking Data...";

        UnityInitializer.AttachToGameObject(Game.UnityManager);
        AWSConfigs.HttpClient = AWSConfigs.HttpClientOption.UnityWebRequest;

        Credentials = new CognitoAWSCredentials(
            "us-east-2:19b8d8df-6d63-4aea-b65c-5914b16519ea", // Identity Pool ID
            RegionEndpoint.USEast2 // Region
            );

        if(!Application.isEditor)
        {
            if (GoogleToken.Length > 0)
            {
                Credentials.AddLogin("accounts.google.com", GoogleToken);
            }
        }

        // Initialize the Cognito Sync client
        syncManager = new CognitoSyncManager(
            Credentials,
            new AmazonCognitoSyncConfig
            {
                RegionEndpoint = RegionEndpoint.USEast2 // Region
            }
        );


        Credentials.GetIdentityIdAsync(delegate (AmazonCognitoIdentityResult<string> result) {
            if (result.Exception != null)
            {
                Debug.Log(result.Exception);
                //Exception!
            }
            
            Game.Data.identityId = result.Response;
            UIManager.instance.LoadingPlayerText.text = "Account ID " + Game.Data.identityId;
            Debug.Log("Account ID " + Game.Data.identityId); 
        });

        SynchronizeAWS("Synchronizing Data...");
    }

    private void SynchronizeAWS(string message)
    {
        // Create a record in a dataset and synchronize with the server

        LocalDataSet = syncManager.OpenOrCreateDataset("PlayerData");

        LocalDataSet.Put("User Name", Social.localUser.userName);
        LocalDataSet.Put("User ID", Social.localUser.id);

        LocalDataSet.OnSyncSuccess += SyncSuccessCallback;
        LocalDataSet.OnSyncFailure += SyncFailureCallback;
        LocalDataSet.OnSyncConflict += SyncConflict;
        LocalDataSet.OnDatasetDeleted = HandleDatasetDeleted;

        LocalDataSet.SynchronizeAsync();
        UIManager.instance.LoadingText.text = message;
    }

    private void SyncSuccessCallback(object sender, SyncSuccessEventArgs e)
    {
        RemoteDataSet = sender as Dataset;

        UIManager.instance.LoadingPlayerText.text = "Sync Successfull";

        Game.Inventory.GalaMiles = ParseIntRecord(RemoteDataSet,"GalaMiles");
        Game.Inventory.SpaceBux = ParseIntRecord(RemoteDataSet, "SpaceBux");
        Game.Inventory.ShipIndex = ParseIntRecord(RemoteDataSet,"ShipIndex");

        SetupOwnedShips();  

        FinishLoading();
    }

    int ParseIntRecord(Dataset sender, string Key)
    {
        int tgm;
        string Value = sender.Get(Key);
        if (Value != null)
        {
            if (int.TryParse(sender.GetRecord(Key).Value, out tgm))
            {
                return tgm;
            }
        }

        LocalDataSet.Put(Key, "0");
        return 0;
    }

    private void SyncFailureCallback(object sender, SyncFailureEventArgs e)
    {
        Debug.Log("*****SyncFailureCallback*****");
        UIManager.instance.LoadingText.text = "Sync Failed!";

        RemoteDataSet = sender as Dataset;
        if (RemoteDataSet.Metadata != null)
        {
            Debug.Log("Sync failed for dataset : " + RemoteDataSet.Metadata.DatasetName);
            Debug.Log(e.Exception);

            UIManager.instance.LoadingPlayerText.text = "";
            if (RemoteDataSet.Records.Count > 0)
            {

                UIManager.instance.LoadingPlayerText.text = string.Concat(UIManager.instance.LoadingPlayerText.text, "\n", "AWS Identity ID: ", Game.Data.identityId);
                //UIManager.instance.LoadingPlayerText.text = string.Concat(UIManager.instance.LoadingPlayerText.text, "\n", "Exception: ", e.Exception);

                UIManager.instance.LoadingText.text = string.Concat("Sync failed for dataset : ", RemoteDataSet.Metadata.DatasetName);

                UIManager.instance.DataIssue.IsVisible = true;

                ScreenCapture.CaptureScreenshot("SHQLogin.png");
                //SynchronizeAWS(string.Concat("Sync failed for dataset : ", dataset.Metadata.DatasetName," Retrying..."));
            }
            else
            {
                UIManager.instance.LoadingPlayerText.text = "No Records in dataset!";
            }
        }
        else
        {
            Debug.Log("Sync failed");
            SynchronizeAWS("Sync failed Retrying...");
        }
    }

    private bool SyncConflict(Dataset dataset, List<SyncConflict> conflicts)
    {
        if (dataset.Metadata != null)
        {
            Debug.LogWarning("Sync conflict " + dataset.Metadata.DatasetName);
        }
        else
        {
            Debug.LogWarning("Sync conflict");
        }
        List<Record> resolvedRecords = new List<Record>();
        foreach (SyncConflict conflictRecord in conflicts)
        {
            // SyncManager provides the following default conflict resolution methods:
            //      ResolveWithRemoteRecord - overwrites the local with remote records
            //      ResolveWithLocalRecord - overwrites the remote with local records
            //      ResolveWithValue - to implement your own logic
            resolvedRecords.Add(conflictRecord.ResolveWithRemoteRecord());
        }
        // resolves the conflicts in local storage
        dataset.Resolve(resolvedRecords);
        // on return true the synchronize operation continues where it left,
        //      returning false cancels the synchronize operation
        return true;
    }

    public bool HandleDatasetDeleted(Dataset dataset)
    {
        Debug.Log(dataset.Metadata.DatasetName + " Dataset has been deleted");
        return false;
    }

    void SaveDatasetToPlayerPrefs(Dataset datasetToSave)
    {
        for (int i = 0; i < datasetToSave.Records.Count; i++)
        {
            Debug.Log(String.Concat("datasetToSave Data ", datasetToSave.Records[i].Key, " ", datasetToSave.Records[i].Value));
            PlayerPrefs.SetString(datasetToSave.Records[i].Key, datasetToSave.Records[i].Value);
        }
    }

    public void KeepLocalData()
    {
        SaveDatasetToPlayerPrefs(LocalDataSet);

        Credentials.Clear();
        Debug.Log("CLEARED");

        LocalDataSet.Put("GalaMiles", PlayerPrefs.GetString("GalaMiles"));
        LocalDataSet.Put("SpaceBux", PlayerPrefs.GetString("SpaceBux"));
        LocalDataSet.Put("ShipIndex", PlayerPrefs.GetString("ShipIndex"));

        Credentials.GetIdentityIdAsync(delegate (AmazonCognitoIdentityResult<string> result) {
            if (result.Exception != null)
            {
                Debug.Log(result.Exception);
                //Exception!
            }

            Game.Data.identityId = result.Response;
            UIManager.instance.LoadingPlayerText.text = "Account ID " + Game.Data.identityId;
            Debug.Log("Account ID " + Game.Data.identityId);
        });

        UIManager.instance.LoadingText.text = "Synchronizing Data...";
        LocalDataSet.SynchronizeAsync();

        UIManager.instance.DataIssue.IsVisible = false;
    }

    public void SaveInventory()
    {
        if(syncManager != null && !Game.instance.IsLoading)
        {
            if (LocalDataSet == null)
            {
                LocalDataSet = syncManager.OpenOrCreateDataset("PlayerData");
            }
            LocalDataSet.Put("GalaMiles", Game.Inventory.GalaMiles.ToString());
            LocalDataSet.Put("SpaceBux", Game.Inventory.SpaceBux.ToString());
            LocalDataSet.Put("ShipIndex", Game.Inventory.ShipIndex.ToString());
        }
    }

    void SetupOwnedShips()
    {
        string Value = RemoteDataSet.Get("OwnedShipIDs");
        if (Value != null)
        {
            StoreManager.instance.SetOwnedShips(RemoteDataSet.GetRecord("OwnedShipIDs").Value);
        }
        else
        {
            SetOwnedShips("0");
        }
    }

    public string GetOwnedShips()
    {
        string Value = RemoteDataSet.Get("OwnedShipIDs");
        if (Value != null)
        {
            return RemoteDataSet.GetRecord("OwnedShipIDs").Value;
        }
        else
        {
            return "0,";
        }
    }

    public void SetOwnedShips(string index)
    {
        string formattedIndex = string.Concat(index, ",");
        LocalDataSet.Put("OwnedShipIDs", formattedIndex);
        StoreManager.instance.SetOwnedShips(formattedIndex);
    }

    public void AddOwnedShip(int index)
    {
        string newOwnedShipsString = string.Concat(FormatShipIndex(index), GetOwnedShips());
        LocalDataSet.Put("OwnedShipIDs", newOwnedShipsString);
        StoreManager.instance.SetOwnedShips(newOwnedShipsString);
    }

    string FormatShipIndex(int indexToFormat)
    {
        return string.Concat(indexToFormat, ",");
    }

    void FinishLoading()
    {
        Game.instance.IsLoading = false;

        Game.instance.CreatePlayerObject();

        UIManager.instance.SetName();

        Game.Achievements.CompleteAchievement("CgkI6qSb6-wUEAIQBA");
    }
}
