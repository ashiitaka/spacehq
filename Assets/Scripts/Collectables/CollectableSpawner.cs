﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableSpawner : MonoBehaviour {

    public static CollectableSpawner instance;
    public List<GameObject> Spawnables = new List<GameObject>();

    bool isActive = true;

    private void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start ()
    {
        StartCoroutine(RespawnObject(Random.Range(5,10)));
	}
	
    public IEnumerator RespawnObject(float spawnTime)
    {
        yield return new WaitForSeconds(spawnTime);
        if(isActive)
        {
            GameObject s = Instantiate(Spawnables[0], transform);
            Vector2 screenpos = Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(50, Screen.width - 50), Screen.height)); //spawn on the screen with a little bit of an indent
            s.transform.SetPositionAndRotation(screenpos, Quaternion.identity);
        }
        StartCoroutine(RespawnObject(Random.Range(5, 10)));
    }

    public void setActive(bool newState)
    {
        isActive = newState;
        if(!isActive)
        {
            foreach(Transform child in this.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
