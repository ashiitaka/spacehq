﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class CollectableObject : MonoBehaviour {

    public int HitsToDestroy;
    public int RewardValue;
    public bool ShouldRotate;
    public ParticleSystem HitParticle;
    public AudioClip HitSFX;
    public AudioClip DestructionSFX;
    public RewardObject RewardDisplay;

    BoxCollider2D boxCollider;
    Rigidbody2D physicsBody;

	// Use this for initialization
	void Awake () {
        boxCollider = GetComponent<BoxCollider2D>();
        physicsBody = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        if(ShouldRotate)
        {
            physicsBody.AddTorque(Random.Range(270, 360));
            physicsBody.rotation = Random.Range(0, 360);
        }
    }

    private void OnMouseDown()
    {
        HitsToDestroy--;
        if (HitParticle != null) ;
        {
            ParticleSystem h = Instantiate(HitParticle);
            h.transform.SetPositionAndRotation(transform.position,Quaternion.identity);
        }

        if(HitsToDestroy <=0)
        {
            RewardObject r = Instantiate(RewardDisplay);
            r.transform.SetPositionAndRotation(transform.position, Quaternion.identity);
            r.Init(string.Concat("+ ", RewardValue));
            Game.Inventory.SpaceBux += RewardValue;
            if (DestructionSFX != null)
            {
                AudioManager.instance.PlaySFX(DestructionSFX);
            }
            Destroy(gameObject);
        }

        if(HitSFX != null)
        {
            AudioManager.instance.PlaySFX(HitSFX);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
