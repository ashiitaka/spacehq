﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoreManager : MonoBehaviour {

    public static StoreManager instance;

    //public object[] ShopShips = Resources.LoadAll("SpaceShips", typeof(PlayerShipScriptable));

    public List<PlayerShipScriptable> ShopShips = new List<PlayerShipScriptable>();

    private void Awake()
    {
        instance = this;
        //ShopShips = Resources.LoadAll("SpaceShips", typeof(PlayerShipScriptable));
    }

    public void SetOwnedShips(string ownedShips)
    {
        for (int i = 0; i < ShopShips.Count; i++) //make sure they are all set not to be owned.
        {
            ShopShips[i].IsOwned = false;
        }

        List<int> OwnedShips = Utilities.StringToArray(ownedShips);

        for (int i = 0; i < OwnedShips.Count; i++)
        {
            if(OwnedShips[i] < ShopShips.Count)
            {
                ShopShips[OwnedShips[i]].IsOwned = true;
            }
            else
            {
                Debug.Log("Invalid Shop Ship Index!");
            }
        }
    }
}
