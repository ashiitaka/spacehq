﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public static UIManager instance;

    [Header("Player Data")]
    public TMPro.TextMeshProUGUI PlayerName;
    public TMPro.TextMeshProUGUI GalaMilesCount;
    public TMPro.TextMeshProUGUI SpaceBuxCount;

    [Header("Main HUD GUI Groups")]
    public GUIGroup MainHUD;
    public GUIGroup MainHUDButtons;
    public GUIGroup ShopUI;
    public GUIGroup NewsUI;
    public GUIGroup SettingsUI;
    public GUIGroup ShipZoomUI;

    [Header("World Map GUI Groups")]
    public GUIGroup WorldMapHUD;
    public GUIGroup AdventureUI;

    [Header("Popups")]
    public GUIGroup NewsPopup;
    public GUIGroup RewardPopup;
    public GUIGroup PurchaseFunnel;
    public GUIGroup ComingSoon;

    [Header("Overlays")]
    public GUIGroup Loading;
    public TMPro.TextMeshProUGUI LoadingText;
    public TMPro.TextMeshProUGUI LoadingPlayerText;
    public GUIGroup DataIssue;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        ResetUIs();
    }

    void ResetUIs()
    {
        MainHUD.IsVisible = true;
        WorldMapHUD.IsVisible = false;
    }

    public void SetName()
    {
        PlayerName.text = string.Concat(Social.localUser.userName);
    }

    public void HideUI()
    {
        MainHUD.IsVisible = false;
    }
    public void CheckMainUI()
    {
        StartCoroutine(visCheck());
    }

    private IEnumerator visCheck()
    {
        yield return new WaitForSeconds(2);
        if (!MainHUD.IsVisible)
        {
            MainHUD.IsVisible = true;
        }
    }
}
