﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;

public class Game : MonoBehaviour {

    public static Game instance;
    public static GameObject UnityManager;
    public static DataManager Data;
    public static InventoryManager Inventory;
    public static AchievementManager Achievements;
    public static AdHandler AdHandlerUnity = new AdHandler();

    public enum availableCameras
    {
        Main,
        ShipZoom,
        Shop,
        WorldMap,
        WorldMapZoom,
    }

    public enum states
    {
        Main,
        ShipZoom,
        Shop,
        WorldMap,
        WorldMapZoom,
    }

    public states state;
    public static states State;

    public static Player PlayerObject;
    public static Camera GameCamera;
    public static CinemachineVirtualCamera MainCamera;
    public static CinemachineVirtualCamera ShipZoomCamera;
    public static CinemachineVirtualCamera ShopCamera;
    public static CinemachineVirtualCamera WorldMapCamera;
    public static CinemachineVirtualCamera WorldMapZoomCamera;

    [Header("Objects")]
    public Player playerObjectPrefab;
    [Header("Cameras")]
    public CinemachineVirtualCamera MainCameraObject;
    public CinemachineVirtualCamera ShipZoomCameraObject;
    public CinemachineVirtualCamera ShopCameraObject;
    public CinemachineVirtualCamera WorldMapCameraObject;
    public CinemachineVirtualCamera WorldMapZoomObject;
    [Header("Environment Objects")]
    public MeshRenderer skyBox;
    public GameObject WorldMap;

    static int tickCount = 0;
    static bool isLoading;

    public bool IsLoading
    {
        get
        {
            return isLoading;
        }
        set
        {
            isLoading = value;
            if(UIManager.instance !=null)
            {
                UIManager.instance.Loading.IsVisible = value;
                UIManager.instance.MainHUD.IsVisible = !value;
            }
        }
    }

    private void Awake()
    {
        instance = this;

        State = states.Main;

        UnityManager = GameObject.Find("UnityManager");

        Inventory = new InventoryManager();
        Achievements = new AchievementManager();
        Data = new DataManager();

        GameCamera = Camera.main;
        MainCamera = MainCameraObject;
        ShipZoomCamera = ShipZoomCameraObject;
        ShopCamera = ShopCameraObject;
        WorldMapCamera = WorldMapCameraObject;
        WorldMapZoomCamera = WorldMapZoomObject;

        SceneManager.sceneLoaded += LoadedScene;
        SceneManager.sceneUnloaded += UnloadedScene;

        Game.instance.WorldMap.SetActive(false);
    }

    // Use this for initialization
    void Start ()
    {
        IsLoading = true;

        UIManager.instance.LoadingText.text = "Logging In...";
        UIManager.instance.LoadingPlayerText.text = "";

        Data.InitialisePlayer();

        StartCoroutine(AutoSave());
    }

    public void LoadScene(string sceneName)
    {
        MoreMountains.CorgiEngine.LoadingSceneManager.LoadScene(sceneName);
    }

    void LoadedScene(Scene scene, LoadSceneMode mode)
    {
        Debug.Log(scene + " loaded");
        Debug.Log(mode);
    }

    void UnloadedScene(Scene scene)
    {
        Debug.Log(scene + " Unloaded");
        Destroy(this);
    }

    public void CreatePlayerObject()
    {
        PlayerObject = Instantiate(playerObjectPrefab);
        PlayerObject.init();

        MainCamera.Follow = PlayerObject.transform;
        ShipZoomCamera.Follow = PlayerObject.transform;
        ShopCamera.Follow = PlayerObject.transform;



        Debug.Log("Player created");
    }

    public static void SwitchCamera(availableCameras newCamera)
    {
        Game.WorldView = false;
        Game.instance.WorldMap.SetActive(false);

        MainCamera.VirtualCameraGameObject.SetActive(false);
        ShopCamera.VirtualCameraGameObject.SetActive(false);
        WorldMapCamera.VirtualCameraGameObject.SetActive(false);
        WorldMapZoomCamera.VirtualCameraGameObject.SetActive(false);

        PlayerObject.IsVisible(true);
        CollectableSpawner.instance.setActive(true);

        switch (newCamera)
        {
            case availableCameras.Main:
                State = states.Main;
                MainCamera.VirtualCameraGameObject.SetActive(true);
                break;
            case availableCameras.ShipZoom:
                State = states.ShipZoom;
                ShipZoomCamera.VirtualCameraGameObject.SetActive(true);
                CollectableSpawner.instance.setActive(false);
                break;
            case availableCameras.Shop:
                State = states.Shop;
                ShopCamera.VirtualCameraGameObject.SetActive(true);
                break;
            case availableCameras.WorldMap:
                State = states.WorldMap;
                Game.instance.WorldMap.SetActive(true);
                WorldMapCamera.VirtualCameraGameObject.SetActive(true);
                Game.WorldView = true;
                CollectableSpawner.instance.setActive(false);
                PlayerObject.IsVisible(false);
                break;
            case availableCameras.WorldMapZoom:
                State = states.WorldMapZoom;
                Game.instance.WorldMap.SetActive(true);
                WorldMapZoomCamera.VirtualCameraGameObject.SetActive(true);
                WorldMapZoomCamera.Follow = WorldMapParent.SelectedPlanet.transform;
                Game.WorldView = true;
                CollectableSpawner.instance.setActive(false);
                PlayerObject.IsVisible(false);
                break;
            default:
                break;
        }
    }

    public static bool WorldView = false;
    // Update is called once per frame
    void FixedUpdate ()
    {
        if(!IsLoading)
        {
            //game tick
            if (tickCount != 60)
            {
                tickCount++;
            }
            else
            {
                Inventory.GalaMiles++;
                tickCount = 0;
            }
        }
        if(WorldView && Game.instance.skyBox.material.GetFloat("_Blend") >= 0)
        {
            Game.instance.skyBox.material.SetFloat("_Blend", Game.instance.skyBox.material.GetFloat("_Blend") - 0.1f);
        }
        else if(!WorldView && Game.instance.skyBox.material.GetFloat("_Blend") <= 1)
        {
            Game.instance.skyBox.material.SetFloat("_Blend", Game.instance.skyBox.material.GetFloat("_Blend") + 0.1f);
        }
    }

    private void OnApplicationPause(bool pause)
    {
        OnApplicationQuit();
    }

    private void OnApplicationQuit()
    {
        if (Data.AWSData != null)
        {
            Data.AWSData.SaveInventory();
        }
    }

    private IEnumerator AutoSave ()
    {
        yield return new WaitForSeconds(60);
        Data.AWSData.SaveInventory();

        AutoSave();
    }
}
