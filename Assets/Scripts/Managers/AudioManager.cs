﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {

    public static AudioManager instance;

    public enum backingTracks
    {
        MainLoop,
    }

    public enum SFX
    {
        ButtonSuccess,
        AchievementUnlocked,
    }

    public AudioSource MusicPlayer;
    List<AudioSource> SFXPlayer = new List<AudioSource>();

    [Header("Mixers")]
    public AudioMixerGroup BackingMusicMixer;
    public AudioMixerGroup SFXMixer;

    [Header("Background Music")]
    public AudioClip MainLoop;

    [Header("Audio Stings SFX")]
    public AudioClip AchievementUnlocked;

    [Header("Button SFX")]
    public AudioClip ButtonSuccess;

    void Awake()
    {
        instance = this;
        MusicPlayer = gameObject.AddComponent<AudioSource>();
        MusicPlayer.outputAudioMixerGroup = BackingMusicMixer;
        MusicPlayer.loop = true;

        SFXPlayer.Add(gameObject.AddComponent<AudioSource>()); ;
        SFXPlayer[0].outputAudioMixerGroup = SFXMixer;
        ChangeBackingMusic(backingTracks.MainLoop);

        IsSFXMuted = false;
    }

    AudioClip GetSFX(SFX sfxToLoad)
    {
        switch (sfxToLoad)
        {
            case SFX.ButtonSuccess:
                return ButtonSuccess;
            case SFX.AchievementUnlocked:
                return AchievementUnlocked;
            default:
                return null;
        }
    }

    public void PlaySFX(SFX sfxToPlay)
    {
        PlaySFX(GetSFX(sfxToPlay));
    }

    public void PlaySFX(AudioClip audioToPlay)
    {
        if(!IsSFXMuted)
        {
            bool hasPlayed = false;

            for (int i = 0; i < SFXPlayer.Count; i++)
            {
                if (!SFXPlayer[i].isPlaying && !hasPlayed)
                {
                    SFXPlayer[i].clip = audioToPlay;
                    SFXPlayer[i].Play();
                    hasPlayed = true;
                }
            }

            if (!hasPlayed)
            {
                SFXPlayer.Add(this.gameObject.AddComponent<AudioSource>());
                int index = SFXPlayer.Count - 1;
                SFXPlayer[index].outputAudioMixerGroup = SFXMixer;
                SFXPlayer[index].clip = audioToPlay;
                SFXPlayer[index].Play();
            }
        }  
    }

    public void ChangeBackingMusic(backingTracks newTrack)
    {
        switch (newTrack)
        {
            case backingTracks.MainLoop:
                if(MusicPlayer.clip != MainLoop)
                {
                    MusicPlayer.clip = MainLoop;
                    MusicPlayer.Play();
                }                    
                break;
            default:
                break;
        }
    }

    public void ToggleMusic()
    {
        bool b = MusicPlayer.mute;
        MusicPlayer.mute = !b;        
    }

    public bool IsSFXMuted;
    public void ToggleSFX()
    {
        for (int i = 0; i < SFXPlayer.Count; i++)
        {
            SFXPlayer[i].mute = !IsSFXMuted;
        }

        IsSFXMuted = !IsSFXMuted;
    }
}
