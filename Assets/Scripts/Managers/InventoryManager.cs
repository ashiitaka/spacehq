﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager {

    
    int galaMiles;
    public int GalaMiles
    {
        get
        {
            return galaMiles;
        }
        set
        {
            galaMiles = value;
            PlayerPrefs.SetInt("GalaMiles", value);
            if(UIManager.instance !=null)
            {
                UIManager.instance.GalaMilesCount.text = string.Concat(galaMiles.ToString());
            }
        }
    }

    int spaceBux;
    public int SpaceBux
    {
        get
        {
            return spaceBux;
        }
        set
        {
            spaceBux = value;
            PlayerPrefs.SetInt("SpaceBux", value);
            if (UIManager.instance != null)
            {
                UIManager.instance.SpaceBuxCount.text = string.Concat(spaceBux.ToString());
            }
            Game.Data.AWSData.SaveInventory();
        }
    }

    int shipIndex;
    public int ShipIndex
    {
        get
        {
            return shipIndex;
        }
        set
        {
            shipIndex = value;
            PlayerPrefs.SetInt("ShipIndex", value);
            if(Game.PlayerObject != null)
            {
                Game.PlayerObject.SetShip(value);
            }
            Game.Data.AWSData.SaveInventory();
        }
    }
}
