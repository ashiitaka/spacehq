﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class AchievementManager {

    public void CompleteAchievement(string AchievementID)
    {
        if(!Application.isEditor)
        {
            Social.ReportProgress(AchievementID, 100.0f, (bool success) => {
                AudioManager.instance.PlaySFX(AudioManager.SFX.AchievementUnlocked);
                // handle success or failure
            });
        }
    }
}
