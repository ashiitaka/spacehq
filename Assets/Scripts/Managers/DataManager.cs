﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;

public class DataManager
{

    public string identityId;
    public Image Avatar = null;

    public Data_AWS AWSData;

    // Use this for initialization
    public void InitialisePlayer()
    {

        if (!Application.isEditor)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
                                        .RequestServerAuthCode(false)
                                        .RequestIdToken()
                                        .Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true;
            PlayGamesPlatform.Activate();
        }

        PlayGamesPlatform.Instance.GetServerAuthCode();

        Social.localUser.Authenticate(ProcessAuthentication);
    }

    void ProcessAuthentication(bool success)
    {
        if (success)
        {
            UIManager.instance.LoadingPlayerText.text = "Authenticated " + Social.Active.ToString();
            Game.Data.AWSData = new Data_AWS();

            if(!Application.isEditor)
            {
                AWSData.GoogleToken = ((PlayGamesLocalUser)Social.localUser).mPlatform.GetIdToken(); //check for google ID
                Debug.Log("Token : " + AWSData.GoogleToken);
                string hasToken;
                if (AWSData.GoogleToken.Length > 0)
                {
                    hasToken = "Has Token";
                }
                else
                {
                    hasToken = "No Token";
                }

                UIManager.instance.LoadingPlayerText.text = string.Concat("Name: ", Social.Active.localUser.userName, "\n",
                                                        "Id: ", Social.localUser.id, "\n",
                                                        "State: ", Social.localUser.state, "\n",
                                                        "Authenticated: ", Social.localUser.authenticated.ToString(), "\n",
                                                        "Token: ", hasToken);
            } 

            Game.Data.AWSData.InitializeAmazonWebServices();
        }
        else
        {
            UIManager.instance.LoadingPlayerText.text = "Not authenticated " + Social.Active.ToString();
            Social.localUser.Authenticate(ProcessAuthentication);
        }
    }
}
