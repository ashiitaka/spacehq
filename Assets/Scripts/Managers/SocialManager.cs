﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SocialManager : MonoBehaviour {

    public static SocialManager instance;
    public Social_Twitter sTwitter; 

    private void Awake()
    {
        instance = this;
        sTwitter = new Social_Twitter();
    }

}
