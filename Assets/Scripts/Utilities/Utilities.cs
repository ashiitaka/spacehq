﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities{

    public static List<int> StringToArray(string newString)
    {
        List<int> intArray = new List<int>();

        for (int i = 0; i < newString.Length; i++)
        {
            int t = new int();
            string s = "";

            while(newString[i].ToString() != ",")
            {
                s = s + newString[i].ToString();
                i++;
            }

            int.TryParse(s,out t);
            if(t >= 0)
            {
                intArray.Add(t);
            }            
        }

        return intArray;
    }
}
