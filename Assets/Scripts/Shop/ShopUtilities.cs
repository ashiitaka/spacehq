﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ShopUtilities {

    public static bool AttemptTransaction(int cost)
    {
        if(Game.Inventory.SpaceBux >= cost)
        {
            Game.Inventory.SpaceBux -= cost;
            return true;
        }
        return false;
    }

    public static bool PurchaseShip(int shipIndex)
    {
        if(AttemptTransaction(StoreManager.instance.ShopShips[shipIndex].Cost))
        {
            Game.Inventory.ShipIndex = shipIndex;
            Game.Data.AWSData.AddOwnedShip(shipIndex);
            StoreManager.instance.ShopShips[shipIndex].IsOwned = true;
            return true;
        }
        else
        {
            return false;
        }
    }
}
