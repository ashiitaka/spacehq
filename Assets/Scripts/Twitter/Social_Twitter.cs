﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TwitterKit.Unity;

public class Social_Twitter {

    public TwitterSession TSession;

    public void Initialize()
    {
        Twitter.Init();
    }

    public void StartLogin()
    {
        TSession = Twitter.Session;
        if (TSession == null)
        {
            Twitter.LogIn(LoginComplete, LoginFailure);
        }
        else
        {
            LoginComplete(TSession);
        }
    }

    public void LoginComplete(TwitterSession session)
    {
        ComposeTweet();
    }

    public void LoginFailure(ApiError error)
    {
        UIManager.instance.CheckMainUI();
        Debug.Log("code=" + error.code + " msg=" + error.message);
    }

    public void StartRequestEmail()
    {
        TSession = Twitter.Session;
        if (TSession != null)
        {
            Twitter.RequestEmail(TSession, RequestEmailComplete, RequestEmailFailure);
        }
        else
        {
            StartLogin();
        }
    }

    public void RequestEmailComplete(string email)
    {
        // Save email
    }

    public void RequestEmailFailure(ApiError error)
    {
        Debug.Log("code=" + error.code + " msg=" + error.message);
    }

    public void ComposeTweet()
    {
        if (TSession != null)
        {
            ScreenCapture.CaptureScreenshot("Screenshot.png");
            Debug.Log("Screenshot location=" + Application.persistentDataPath + "/Screenshot.png");
            string imageUri = "file://" + Application.persistentDataPath + "/Screenshot.png";
            Twitter.Compose(TSession, imageUri, "@SpaceHQGame Requesting...", new string[] { "#SpaceHQGame" },
                (string tweetId) => { UIManager.instance.MainHUD.IsVisible = true;
                                      Game.Achievements.CompleteAchievement("CgkI6qSb6-wUEAIQBQ");
                                      Debug.Log("Tweet Success, tweetId = " + tweetId); },
                (ApiError error) => { UIManager.instance.MainHUD.IsVisible = true;
                                      Debug.Log("Tweet Failed " + error.message); },
                () => { UIManager.instance.MainHUD.IsVisible = true;
                        Debug.Log("Compose cancelled"); }
             );
        }

        UIManager.instance.CheckMainUI();
    }
}
