﻿using UnityEngine;
using UnityEngine.Advertisements;

public class AdHandler {

#if UNITY_IOS
    private string gameId = "1582430";
#elif UNITY_ANDROID
    private string gameId = "1582429";
#endif

    public void ShowRewardedVideo()
    {
        ShowOptions options = new ShowOptions();
        options.resultCallback = HandleShowResult;

        Advertisement.Show("rewardedVideo", options);
    }

    public void HandleShowResult(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            UIManager.instance.RewardPopup.IsVisible = true;
            Game.Inventory.SpaceBux += 10;
        }
        else if (result == ShowResult.Skipped)
        {
            Debug.LogWarning("Video was skipped - Do NOT reward the player");

        }
        else if (result == ShowResult.Failed)
        {
            Debug.LogError("Video failed to show");
        }
    }
}
